from flask.cli import FlaskGroup

from project import create_app, db
from project.api.models import Book


app = create_app()
cli = FlaskGroup(create_app=create_app)


@cli.command('recreate_db')
def recreate_db():
    db.drop_all()
    db.create_all()
    db.session.commit()


@cli.command('seed_db')
def seed_db():
    """Seeds the database."""
    db.session.add(Book(
        name='Alpha',
        address='Alpha\'s Home',
        phone='1111111'
    ))
    db.session.add(Book(
        name='Bravo',
        address='Bravo\'s Home',
        phone='222222'
    ))
    db.session.add(Book(
        name='Charlie',
        address='Charlie\'s Home',
        phone='333333'
    ))
    db.session.commit()


if __name__ == '__main__':
    cli()
