import os

from flask import Blueprint, jsonify, request

from project.api.models import Book
from project import db


books_blueprint = Blueprint('books', __name__)


@books_blueprint.route('/books', methods=['GET', 'POST'])
def all_books():
    response_object = {
        'status': 'success',
        'container_id': os.uname()[1]
    }
    if request.method == 'POST':
        post_data = request.get_json()
        name = post_data.get('name')
        address = post_data.get('address')
        phone = post_data.get('phone')
        db.session.add(Book(name=name, address=address, phone=phone))
        db.session.commit()
        response_object['message'] = 'Address Book added!'
    else:
        response_object['books'] = [book.to_json() for book in Book.query.all()]
    return jsonify(response_object)


@books_blueprint.route('/books/ping', methods=['GET'])
def ping():
    return jsonify({
        'status': 'success',
        'message': 'pong!',
        'container_id': os.uname()[1]
    })


@books_blueprint.route('/books/<book_id>', methods=['PUT', 'DELETE'])
def single_book(book_id):
    response_object = {
      'status': 'success',
      'container_id': os.uname()[1]
    }
    book = Book.query.filter_by(id=book_id).first()
    if request.method == 'PUT':
        post_data = request.get_json()
        book.name = post_data.get('name')
        book.address = post_data.get('address')
        book.phone = post_data.get('phone')
        db.session.commit()
        response_object['message'] = 'Address Book updated!'
    if request.method == 'DELETE':
        db.session.delete(book)
        db.session.commit()
        response_object['message'] = 'Address Book removed!'
    return jsonify(response_object)


if __name__ == '__main__':
    app.run()
